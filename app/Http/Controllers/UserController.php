<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Role;
use Hash;
use DB;
use Auth;
use Datatables;
use Yajra\Datatables\Html\Builder;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        $username = Auth::user()->email;
        //var_dump($username);exit;
        $data['a'] = DB::select("select c.kdprop, c.kdkab, c.nmkab, c.nmprop from 
users a
join wilayah_user b
on  a.id = b.user_id
join daftarwilayahbps c
on b.wilayah_id = c.id_wilayah
where a.email = '".$username."'");
        //var_dump($data['a']);exit;

return view('vendor.adminlte.home', $data);
          
    }

    public function getUserData(){
        

          $username = Auth::user()->email;
        //var_dump($username);exit;
        $data['a'] = DB::select("select c.kdprop, c.kdkab, c.nmkab, c.nmprop from 
users a
join wilayah_user b
on  a.id = b.user_id
join daftarwilayahbps c
on b.wilayah_id = c.id_wilayah
where a.email = '".$username."'");

        $kdprop = $data['a'][0]->kdprop;
        $kdkab = $data['a'][0]->kdkab;
       // var_dump($kdprop.$kdkab);exit;
        $model = DB::select("SELECT A.*,B.NMKEC, B.NMDESA, B.KDPROP, B.KDKAB,
C.NAME, C.EMAIL, C.PHONE, C.ID, C.nik
FROM 
WILAYAH_USER A
JOIN DAFTARWILAYAHBPS B
ON A.WILAYAH_ID = B.ID_WILAYAH
LEFT JOIN 
USERS C
ON A.USER_ID = C.ID
where b.kdprop = ".$kdprop." and b.kdkab = ".$kdkab.""
);

        return Datatables::of($model)
            ->addColumn('action', function ($user) {
                return '<a href="users/'.$user->nomorslot.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>  <a href="' . route('users.destroy', $user->nomorslot) . '"class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';})
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name','id');
         return view('user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required|min:6',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['api_token'] = str_random(100);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }
        return redirect()->route('users.index')
        ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($noSlot)
    {
//          $user = DB::select("select a.*, b.*
// from 
// wilayah_user a
// left join 
// users b
// on a.user_id = b.id
// where nomorslot = '".$noSlot."'")->first();
         
         $user = DB::table('wilayah_user')->select('users.phone','users.name','wilayah_user.wilayah_id','wilayah_user.nomorslot')->leftjoin('users','users.id','=','wilayah_user.user_id')->where('nomorslot',$noSlot)->first();
         //var_dump($data['user']->wilayah_id);exit;

         // print_r($user);

         // $roles = Role::pluck('display_name','id');
         // $userRole = $user->roles->pluck('id','id')->toArray();
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nik' => 'required'
            
        ]);

         $input = $request->all();


        $ceknip = DB::table('users')->where('nik', $input['nik'])->first();
        if(!empty($ceknip)){
           DB::table('wilayah_user')->where('nomorslot', $id)->update(array('user_id' => $ceknip->id));
           redirect()->route('users.index')
                        ->with('success','User updated successfully');
        }else{
             $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'
            
            ]);

            $pwd = substr($id,0,10);
            $input['password'] = Hash::make($pwd);
            $input['api_token'] = str_random(100);
            $user = User::create($input);
        $wilayah = DB::table('wilayah_user')->where('nomorslot', $id)->update(array('user_id' => $user->id));
        }
       //$user = User::create($input);
        //$wilayah = DB::table('wilayah_user')->where('nomorslot', $id)->update(array('user_id' => $user->id));

       // var_dump($wilayah);exit;
        // if(!empty($input['password'])){ 
        //     $input['password'] = Hash::make($input['password']);
        // }else{
        //     $input = array_except($input,array('password'));    
        // }

        // $user = User::find($id);
        // $user->update($input);
        // DB::table('role_user')->where('user_id',$id)->delete();

        
        // foreach ($request->input('roles') as $key => $value) {
        //     $user->attachRole($value);
        // }

        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         User::find($id)->delete();
         return redirect()->route('users.index')
                            ->with('success','User deleted successfully');;
    }
}
