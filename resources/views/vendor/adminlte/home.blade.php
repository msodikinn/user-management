@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.user') }}
@endsection

@section('contentheader_title')
User Management
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body">
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
              <p>{{ $message }}</p>
            </div>
          @endif
          @foreach($a as $d)
			    <p>{{ $d->nmprop }}</p>
			    <p>{{ $d->nmkab }}</p>
			@endforeach
          <div class="table-responsive col-sm-12">
                    <table class="table table-hover" id="user-table">
                        <thead>
                            <tr>
                                <th>Kecamatan</th>
                                <th>Desa</th>
                                <th>No Slot</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="table-hover">
                           
                        </tbody>
                    </table>
                </div>

                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
 <script>
  $(function() {
    var table = $('#user-table').DataTable({
           serverSide: true,
           // "bSort": false,
           // "bPaginate": false,
           //  "bInfo": false,
           //   "bFilter": false,
            ajax: '{{ url("user/data") }}',
            columns: [
                //{ data: 'action',orderable: false, searchable: false},
              { data: 'NMKEC',className: "namaprop"},
              { data: 'NMDESA',className: "sum kanan"},
              { data: 'nomorslot',className: "sum kanan"},
              { data: 'NAME',className: "sum kanan"},
              { data: 'EMAIL',className: "sum kanan"},
              { data: 'PHONE',className: "kanan" },
              { data: 'action',className: "kanan" },
              
            ]

        });

   });
  </script>
 @endsection