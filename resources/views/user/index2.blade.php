@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.user') }}
@endsection

@section('contentheader_title')
User Management
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">User List
                    <span class="pull-right">
                     <a class="btn btn-xs btn-success" href="{{
                    route('users.create') }}"><i class="fa fa-plus"> Create New</i></a>
                     </span>
                 </h3>
            </div>
            <div class="panel-body">
             @if ($message = Session::get('success'))
                <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>{{ $message }}</p>
                </div>
            @endif
             {!! $html->table(['class'=>'table table-striped table-bordered']) !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
{!! $html->scripts() !!}

<script type="text/javascript">
$(document).ready(function () {
$(document.body).on('click', '.js-submit-confirm', function (event) {
event.preventDefault()
var $form = $(this).closest('form')
var $el = $(this)
var text = $el.data('confirm-message') ? $el.data('confirm-message') : 'Kamu tidak akan bisa membatalkan proses ini!'
swal({
title: 'Are you sure??',
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#DD6B55',
confirmButtonText: 'Yes, delete it!',
cancelButtonText: 'cancel',
closeOnConfirm: true
},
function () {
$form.submit()
})

})
})

</script>
@endsection