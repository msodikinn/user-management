@extends('adminlte::layouts.app')

@section('htmlheader_title')
	user-create
@endsection

@section('contentheader_title')
  User Create
@endsection



@section('main-content')
	<div class="row" style="margin-bottom: 25px">
        <div class="col-md-12">
            <a href="{{ url()->previous() }}" class="btn btn-info" type="button">
                <span class="fa fa-arrow-left"></span>
                Kembali</a>
        </div>
 </div>
 @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">create new</h3>
               </div>
                <div class="box-body">
        {!! Form::open(['route' => 'users.store'])!!}
          @include('user.form')
        {!! Form::close() !!}
        </div>
      </div>
    </div>
      
  </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(document).ready(function() {
    $(".multiple").select2({
  allowClear: true
});
  });
  </script>
@endsection
