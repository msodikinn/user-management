@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home')}}
@endsection

@section('contentheader_title')
  Edit Create
@endsection

@section('main-content')
	<div class="row" style="margin-bottom: 25px">
        <div class="col-md-12">
            <a href="{{ url()->previous() }}" class="btn btn-info" type="button" >
                <span class="fa fa-arrow-left"></span>
                Kembali</a>
        </div>
 </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Edit Wilayah {{ $user->wilayah_id }}</h3>
               </div>
                <div class="box-body">
        {!! Form::model($user, ['route' => ['users.update', $user->nomorslot],'method' =>'patch'])!!}
          @if (count($errors) > 0)
          <ul>
          @foreach ($errors->all() as $error)
          <li class="alert alert-danger">{{ $error }}</li>
          @endforeach
          </ul>
          @endif
           @include('user.form', ['model' => $user])
        {!! Form::close() !!}
        </div>
      </div>
    </div>
      
  </div>
@endsection
